package sample;

import javafx.concurrent.Task;
import javafx.css.Match;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;

import com.sun.management.OperatingSystemMXBean;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

import java.lang.reflect.Member;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller implements Initializable
{
    @FXML
    private AnchorPane main_form;
    @FXML
    private GridPane gridpane;
    @FXML
    private Menu menuBar;
    @FXML
    private javafx.scene.control.Button btn_dwn;
    @FXML
    private ProgressBar dwnProgress;
    @FXML
    private CheckBox cChrome;
    @FXML
    private CheckBox cDiscord;
    @FXML
    private CheckBox c7zip;
    @FXML
    private Label dwn_lbl;
    private long size = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
//        CheckBox checkBox1 = new CheckBox("Google Chrome");
//        AnchorPane.setTopAnchor(checkBox1, 80.0);
//        AnchorPane.setRightAnchor(checkBox1, 250.0);
//        main_form.getChildren().addAll(checkBox1);
        dwn_lbl.setVisible(false);
    }

    private static final String[] EDITIONS = {
            "家用版", "專業版", "企業版"
    };

    private static final String[][] SETUPS = {
            {"https://drive.google.com/uc?id=1lwyFAV-lWnKSFoOBx8NLVgiKmAucgK0U&authuser=1&export=download", "ChromeSetup"},
            {"https://drive.google.com/uc?id=1kgstKA8GqeRVyhWLMEn8PrnSCDwh8Qwh&authuser=1&export=download", "DiscordSetup"},
            {"https://drive.google.com/uc?id=1yk4piL8q0Nyyf0uPqzoWv243-fiaVqHY&authuser=1&export=download", "7zipSetup"}
    };

    private static final boolean[] Install = {
            false, false, false
    };

    public static String findSysInfo(String term)
    {
        try
        {
            Runtime rt = Runtime.getRuntime();
            //Process pr = rt.exec("CMD /C SYSTEMINFO | FINDSTR /B /C:\"" + term + "\"");
            //Process pr = rt.exec("wmic os get Caption /value");
            //BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            ProcessBuilder builder = new ProcessBuilder(
                    "cmd.exe", "/c", "wmic OS get \"" + term + "\"");
            builder.redirectErrorStream(true);
            Process p = builder.start();
            String result = getStringFromInputStream(p.getInputStream());
            return result;
        } catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
        return "";
    }

    public static String getEdition()
    {
        String osName = findSysInfo("Caption");
        if (!osName.isEmpty())
        {
            for (String edition : EDITIONS)
            {
                if (osName.contains(edition))
                {
                    return edition;
                }
            }
        }
        return null;
    }

    public void SystemMenuClick() throws IOException
    {
        OperatingSystemMXBean osmb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        java.lang.management.ThreadMXBean t = ManagementFactory.getThreadMXBean();

        String nameOS = "os.name";
        String versionOS = "os.version";
        String architectureOS = "sun.arch.data.model";
        String builderOS = "";
        String totalRAM;
        String nameCPU;
        String diskInfo = "";
        //String architectureOS = "os.arch";

        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "wmic OS get BuildNumber /value");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        String result = getStringFromInputStream(p.getInputStream());

        builderOS = result;
        builderOS = builderOS.replaceAll("[^0-9]", "");
        totalRAM = (osmb.getTotalPhysicalMemorySize() / 1024 / 1024) + " MB";

        builder = new ProcessBuilder(
                "cmd.exe", "/c", "wmic cpu get Name /value");
        builder.redirectErrorStream(true);
        p = builder.start();
        result = getStringFromInputStream(p.getInputStream());
        nameCPU = result;
        nameCPU = nameCPU.substring(nameCPU.lastIndexOf("=") + 1);

        File[] roots = File.listRoots();
        for (File file : roots)
        {
            diskInfo += file.getPath();
            diskInfo += " 未使用：" + (file.getFreeSpace() / 1024 / 1024 / 1024) + " GB \t";
            diskInfo += " 可用：" + (file.getUsableSpace() / 1024 / 1024 / 1024) + " GB \t";
            diskInfo += " 總空間：" + (file.getTotalSpace() / 1024 / 1024 / 1024) + " GB \t";
            diskInfo += "\r\n";
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Computer Information");
        alert.setHeaderText(null);
        alert.setContentText(
                "The information about OS:" + "\r\n" +
                        "處理器: " + nameCPU + "\r\n" +
                        "作業系統: " + System.getProperty(nameOS) + " " +
                        getEdition() + " " + builderOS + " - " +
                        System.getProperty(architectureOS) + "位元" + "\r\n" +
                        "記憶體空間: " + totalRAM + "\r\n" +
                        "硬碟資訊:\r\n" + diskInfo
        );
        alert.setResizable(false);
        alert.getDialogPane().setPrefSize(480, 200);
        alert.showAndWait();
//        alert.showAndWait().ifPresent(rs -> {
//            if (rs == ButtonType.OK) {
//                System.out.println("Pressed OK.");
//            }
//        });
    }

    private static String getStringFromInputStream(InputStream is)
    {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try
        {

            br = new BufferedReader(new InputStreamReader(is, "big5"));
            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public void DownloadClick()
    {
        boolean checkDouble = false;
        if ((cChrome.isSelected() && c7zip.isSelected()) || (cChrome.isSelected() && cDiscord.isSelected()) || (cDiscord.isSelected() && c7zip.isSelected()))
        {
            checkDouble = true;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("錯誤");
            alert.setHeaderText(null);
            alert.setContentText(
                    "只能選擇一項！"
            );
            alert.setResizable(false);
            alert.getDialogPane().setPrefSize(250, 100);
            alert.showAndWait();
        }
        else
        {
            checkDouble = false;
        }

        if (!checkDouble)
            if (cChrome.isSelected())
            {
                Task<Void> task = new DownloadTask(SETUPS[0][0]);
                dwnProgress.progressProperty().bind(task.progressProperty());
                dwn_lbl.setVisible(true);
                dwn_lbl.setText("下載中...");

                Thread thread = new Thread(task);
                thread.setDaemon(true);
                thread.start();
            }
            else if (cDiscord.isSelected())
            {
                Task<Void> task = new DownloadTask(SETUPS[1][0]);
                dwnProgress.progressProperty().bind(task.progressProperty());
                dwn_lbl.setVisible(true);
                dwn_lbl.setText("下載中...");

                Thread thread = new Thread(task);
                thread.setDaemon(true);
                thread.start();
            }
            else if (c7zip.isSelected())
            {
                Task<Void> task = new DownloadTask(SETUPS[2][0]);
                dwnProgress.progressProperty().bind(task.progressProperty());
                dwn_lbl.setVisible(true);
                dwn_lbl.setText("下載中...");

                Thread thread = new Thread(task);
                thread.setDaemon(true);
                thread.start();
            }
            else
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("錯誤");
                alert.setHeaderText(null);
                alert.setContentText(
                        "請選擇一項要安裝的軟體！"
                );
                alert.setResizable(false);
                alert.getDialogPane().setPrefSize(250, 100);
                alert.showAndWait();
            }
    }

    private class DownloadTask extends Task<Void>
    {

        private String url;

        public DownloadTask(String url)
        {
            this.url = url;
        }

        @Override
        protected Void call() throws Exception
        {
            //String ext = url.substring(url.lastIndexOf("."), url.length());
            URLConnection connection = new URL(url).openConnection();
            connection.connect();
            long fileLength = connection.getContentLengthLong();
            //Commit
            try (InputStream is = connection.getInputStream();
                 OutputStream os = Files.newOutputStream(Paths.get("Setup/Setup.exe"));)
            {
                long nread = 0L;
                byte[] buf = new byte[8192];
                int n;
                while ((n = is.read(buf)) > 0)
                {
                    os.write(buf, 0, n);
                    nread += n;
                    updateProgress(nread, fileLength);
                }
            }

            return null;
        }

        @Override
        protected void failed()
        {
            dwn_lbl.setText("下載失敗!");
        }

        @Override
        protected void succeeded()
        {
            dwn_lbl.setText("下載完成!");
            updateProgress(100, 100);
            String command = "Setup\\Setup.exe";
            System.out.println(command);
            try
            {
                Runtime.getRuntime().exec("cmd /c \"" + command + "\" /S");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
